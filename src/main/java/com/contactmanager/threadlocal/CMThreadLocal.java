package com.contactmanager.threadlocal;

import java.util.Properties;

import com.contactmanager.db.DBConnector;

/**
 * @author siva-4578
 *
 */
public class CMThreadLocal {

	private static final ThreadLocal<CMThreadModel> THREADLOCAL = new ThreadLocal<CMThreadModel>();

	public static void initialize(CMThreadModel threadModel) {
		THREADLOCAL.set(threadModel);
	}

	public static void clear() {
		THREADLOCAL.remove();
	}

	public static void setRootPath(String rootPath) {
		THREADLOCAL.get().setRootPath(rootPath);
	}

	public static String getRootPath() {
		return THREADLOCAL.get().getRootPath();
	}

	public static void setConfigProperties(Properties configProperties) {
		THREADLOCAL.get().setConfigProperties(configProperties);
	}

	public static Properties getConfigProperties() {
		return THREADLOCAL.get().getConfigProperties();
	}

	public static void setConnector(DBConnector connector) {
		THREADLOCAL.get().setConnector(connector);
	}

	public static DBConnector getConnector() {
		return THREADLOCAL.get().getConnector();
	}
}