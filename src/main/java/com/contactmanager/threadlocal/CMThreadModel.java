package com.contactmanager.threadlocal;

import java.util.Properties;

import com.contactmanager.db.DBConnector;

/**
 * @author siva-4578
 *
 */
public class CMThreadModel {

	private String rootPath;
	private Properties configProperties;
	private DBConnector connector;

	private CMThreadModel() {

	}
	
	public static CMThreadModel getInstance() {
		return new CMThreadModel();
	}

	protected String getRootPath() {
		return rootPath;
	}

	protected void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	} 

	protected Properties getConfigProperties() {
		return configProperties;
	}

	protected void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

	protected DBConnector getConnector() {
		return connector;
	}

	protected void setConnector(DBConnector connector) {
		this.connector = connector;
	}
}