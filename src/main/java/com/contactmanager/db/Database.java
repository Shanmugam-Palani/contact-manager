package com.contactmanager.db;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;

import com.contactmanager.common.constants.ContactManagerCommonConstants;
import com.contactmanager.common.utils.IOUtils;
import com.contactmanager.common.utils.LoggerUtil;
import com.contactmanager.threadlocal.CMThreadLocal;

/**
 * @author siva-4578
 *
 */
public class Database implements IDatabase {

	private static final Logger LOGGER = Logger.getLogger(Database.class.getSimpleName());

	private static Database database = null;

	private Database() {

	}

	public static Database getInstance() {
		if(database == null) {
			database = new Database();
		}
		return database;
	}

	@Override
	public void createDatabase() throws Exception {
		Statement statement = null;
		try {
			String query = "CREATE DATABASE " + ContactManagerCommonConstants.DATABASE_NAME;
			statement = CMThreadLocal.getConnector().getConnection(true).createStatement();
			statement.executeUpdate(query);
			LOGGER.info("Database created");
		} catch(Exception exception){
			LOGGER.debug(LoggerUtil.formatMessage("createDatabase ::: {0}", exception.getMessage()), exception);
			throw exception;
		} finally {
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}

	@Override
	public void dropDatabase() throws Exception {
		Statement statement = null;
		try {
			String query = "DROP DATABASE " + ContactManagerCommonConstants.DATABASE_NAME;
			statement = CMThreadLocal.getConnector().getConnection(true).createStatement();
			statement.executeUpdate(query);
			LOGGER.info("Database dropped");
		} catch(Exception exception){
			LOGGER.debug(LoggerUtil.formatMessage("dropDatabase ::: {0}", exception.getMessage()), exception);
			throw exception;
		} finally {
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}

	@Override
	public void populateTables(List<String> tableCreateQueries) throws Exception {
		if(tableCreateQueries == null || tableCreateQueries.isEmpty()) {
			return;
		}
		Statement statement = null;
		try {
			statement = CMThreadLocal.getConnector().getConnection().createStatement();
			for(String query : tableCreateQueries) {
				statement.executeUpdate(query);
			}
			LOGGER.info("Tables populated");
		} catch(Exception exception){
			LOGGER.debug(LoggerUtil.formatMessage("populateTables ::: {0}", exception.getMessage()), exception);
			throw exception;
		} finally {
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}

	@Override
	public boolean isDatabaseExists() throws Exception {
		ResultSet record = null;
		try {
			record = CMThreadLocal.getConnector().getConnection(true).getMetaData().getCatalogs();
			while (record.next()) {
				String databaseName = record.getString(1);
				if (ContactManagerCommonConstants.DATABASE_NAME.contentEquals(databaseName)) {
					return true;
				}
			}
		} catch(Exception exception){
			LOGGER.debug(LoggerUtil.formatMessage("isDatabaseExists ::: {0}", exception.getMessage()), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(record);
			CMThreadLocal.getConnector().close();
		}
		return false;
	}
}