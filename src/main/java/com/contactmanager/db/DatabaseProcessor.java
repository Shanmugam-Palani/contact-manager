package com.contactmanager.db;

import com.contactmanager.common.constants.FilePaths;
import com.contactmanager.common.utils.IOUtils;
import com.contactmanager.common.utils.XmlParser;

/**
 * @author siva-4578
 *
 */
public class DatabaseProcessor {

	public static void createDatabase() throws Exception {
		IDatabase database = Database.getInstance();
		if (database.isDatabaseExists()) {
			throw new Exception("Contact manager database has been already populated.");
		}
		database.createDatabase();
		populateTables();
	}

	public static void dropDatabase() throws Exception {
		IDatabase database = Database.getInstance();
		database.dropDatabase();
	}

	private static void populateTables() throws Exception {
		XmlParser.Builder builder = XmlParser.Builder.getInstance();
		builder.setFilePath(IOUtils.getFilePath(FilePaths.SCHEMA_CONFIGURATION));
		XmlParser parser = builder.build();

		IDatabase database = Database.getInstance();
		database.populateTables(parser.getTableSchemata());
	}
}