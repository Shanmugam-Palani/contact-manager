package com.contactmanager.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.contactmanager.common.constants.ContactManagerCommonConstants;
import com.contactmanager.common.utils.IOUtils;
import com.contactmanager.common.utils.LoggerUtil;
import com.mysql.cj.jdbc.Driver;

/**
 * @author siva-4578
 *
 */
public class DBConnector {

	private static final Logger LOGGER = Logger.getLogger(DBConnector.class.getSimpleName());

	private String server;
	private String username;
	private String password;
	private Connection connection;

	private DBConnector(Builder builder) {
		this.server = builder.server;
		this.username = builder.username;
		this.password = builder.password;
	}

	private void initializeServer() throws SQLException {
		try {
			DriverManager.registerDriver(new Driver());
			this.connection = DriverManager.getConnection(server, username, password);
		} catch(SQLException exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Exception ::: initializeServer ::: {0}", exception.getMessage()), exception);
			throw exception;
		}
	}

	private void initializeDB() throws SQLException {
		try {
			DriverManager.registerDriver(new Driver());
			this.connection = DriverManager.getConnection(server + ContactManagerCommonConstants.DATABASE_NAME, username, password);
		} catch(SQLException exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Exception ::: initializeDB ::: {0}", exception.getMessage()), exception);
			throw exception;
		}
	}

	/**
	 * Getting connection to database
	 * @return SQL Connection
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return getConnection(false);
	}

	/**
	 * Getting connection to MySQL server
	 * @param isServer
	 * @return SQL Connection
	 * @throws SQLException
	 */
	public Connection getConnection(boolean isServer) throws SQLException {
		if(isServer) {
			initializeServer();
		} else {
			initializeDB();
		}
		return connection;
	}

	public void close() {
		IOUtils.closeConnection(connection);
	}

	public static class Builder {
		private String server;
		private String username;
		private String password;

		public static Builder getInstance() {
			return new Builder();
		}
		public DBConnector build() {
			return new DBConnector(this);
		}
		public void setServerUrl(String server) {
			this.server = server;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public void setPassword(String password) {
			this.password = password;
		}
	}
}