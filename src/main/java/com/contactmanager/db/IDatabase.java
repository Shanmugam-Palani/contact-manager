package com.contactmanager.db;

import java.util.List;

/**
 * @author siva-4578
 *
 */
public interface IDatabase {

	/**
	 * Setting up database
	 */
	public void createDatabase() throws Exception;

	/**
	 * Removing database
	 */
	public void dropDatabase() throws Exception;

	/**
	 * Creating table schemata
	 * @throws Exception
	 */
	public void populateTables(List<String> tableCreateQueries) throws Exception;
	
	/**
	 * Verify whether database already exists or not
	 * @return {@link Boolean}
	 * @throws Exception
	 */
	public boolean isDatabaseExists() throws Exception;
}