package com.contactmanager.server;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;

import com.contactmanager.threadlocal.CMThreadLocal;

/**
 * @author siva-4578
 *
 */
public class ServiceFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(ServiceFilter.class);

	private FilterConfig filterConfig;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			ServiceStartupHandler.initializeThreadLocal(this.filterConfig.getServletContext().getRealPath("/"));
			chain.doFilter(request, response);
		} finally {
			CMThreadLocal.clear();
		}
	}

	@Override
	public void destroy() {
		LOGGER.info("ServiceFilter destroyed");
	}
}