package com.contactmanager.server;

import java.io.File;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.contactmanager.common.constants.FilePaths;
import com.contactmanager.common.utils.IOUtils;
import com.contactmanager.db.DBConnector;
import com.contactmanager.joda.JodaAPI;
import com.contactmanager.threadlocal.CMThreadLocal;
import com.contactmanager.threadlocal.CMThreadModel;

/**
 * @author siva-4578
 *
 */
public class ServiceStartupHandler {

	protected static void initialize(ServletContext servletContext) throws Exception {
		initialize(servletContext.getRealPath("/"));
	}

	private static void initialize(String rootPath) throws Exception {
		initializeLog4j(rootPath);
	}

	private static void initializeLog4j(String rootPath) throws Exception {
		String logFilePath = IOUtils.getFilePath(rootPath, FilePaths.LOGS_HOME) + File.separator + "contactmanager_" + JodaAPI.format(JodaAPI.getCurrentTime(), true) + ".log";
		File logFile = new File(logFilePath);
		if(!logFile.exists()) {
			logFile.createNewFile();
		}

		Properties log4jProperties = IOUtils.loadProperties(IOUtils.getFilePath(rootPath, FilePaths.LOG4J_PROPERTIES));
		log4jProperties.put("log4j.appender.CMFileAppender.File", logFilePath);
		PropertyConfigurator.configure(log4jProperties);
	}

	public static void initializeThreadLocal(String rootPath) {
		try {
			CMThreadLocal.initialize(CMThreadModel.getInstance());
			CMThreadLocal.setRootPath(rootPath);
			CMThreadLocal.setConfigProperties(IOUtils.loadProperties(IOUtils.getFilePath(FilePaths.CONFIGURATION_PROPERTIES)));

			DBConnector.Builder builder = DBConnector.Builder.getInstance();
			builder.setServerUrl(CMThreadLocal.getConfigProperties().getProperty("mysql.server.url", "jdbc:mysql://localhost:3306/"));
			builder.setUsername(CMThreadLocal.getConfigProperties().getProperty("mysql.server.username", "root"));
			builder.setPassword(CMThreadLocal.getConfigProperties().getProperty("mysql.server.password", ""));
			CMThreadLocal.setConnector(builder.build());
		} catch(Exception exception) {
			Logger.getLogger(ServiceStartupHandler.class).debug("Exception in initializeThreadLocal", exception);
		}
	}
}