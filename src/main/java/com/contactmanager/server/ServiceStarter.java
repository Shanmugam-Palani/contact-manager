package com.contactmanager.server;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

/**
 * @author siva-4578
 *
 */
@WebServlet(urlPatterns = "/ServiceStarter", loadOnStartup = 1)
public class ServiceStarter extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(ServiceStarter.class.getSimpleName());

	private static final long serialVersionUID = 1L;

	public ServiceStarter() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		try {
			ServiceStartupHandler.initialize(config.getServletContext());
			LOGGER.info("Service started");
		} catch (Exception exception) {
			throw new ServletException(exception.getMessage(), exception);
		}
	}
}