package com.contactmanager.common.utils;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author siva-4578
 *
 */
public class ResponseUtil {

	private ResponseUtil() {

	}

	public static Response getSuccessResponse(String requestType, JSONObject response) {
		if (response == null) {
			response = new JSONObject();
		}
		JSONObject responseJson = new JSONObject();
		responseJson.put(requestType, new JSONArray().put(response));

		return Response.ok().entity(responseJson.toString()).build();
	}

	public static Response getSuccessResponse(String requestType, JSONArray response) {
		if (response == null) {
			response = new JSONArray();
		}
		JSONObject responseJson = new JSONObject();
		responseJson.put(requestType, response);

		return Response.ok().entity(responseJson.toString()).build();
	}
	
	public static Response getSuccessResponse(String message) {
		JSONObject responseJson = new JSONObject();
		responseJson.put("message", message);

		return Response.ok().entity(responseJson.toString()).build();
	}

	public static Response getFailureResponse(Throwable throwable) {
		JSONObject response = new JSONObject();
		response.put("error", throwable.getMessage() != null ? throwable.getMessage() : throwable.getClass().getName());
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response.toString()).build();
	}
}