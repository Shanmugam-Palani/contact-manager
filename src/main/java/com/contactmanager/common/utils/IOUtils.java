package com.contactmanager.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.contactmanager.common.constants.FilePaths;
import com.contactmanager.threadlocal.CMThreadLocal;

/**
 * @author siva-4578
 *
 */
public class IOUtils {

	private static final Logger LOGGER = Logger.getLogger(IOUtils.class.getSimpleName());

	public static String getFilePath(final FilePaths path) throws Exception {
		return getFilePath(CMThreadLocal.getRootPath(), path);
	}

	public static String getFilePath(final String rootPath, final FilePaths path) throws Exception {
		return (rootPath.endsWith(File.separator)) ? rootPath + path.getPath() : rootPath + File.separator + path.getPath();
	}

	public static String resolvePath(final String path) throws Exception {
		return resolvePath(CMThreadLocal.getRootPath(), path);
	}

	public static String resolvePath(final String rootPath, final String path) throws Exception {
		return (rootPath.endsWith(File.separator)) ? rootPath + path : rootPath + File.separator + path;
	}

	public static void closeConnection(Connection connection) {
		try {
			if(connection != null) {
				connection.close();
			}
		} catch (Exception exception) {
			LOGGER.warn(LoggerUtil.formatMessage("Exception ::: {0}", exception.getMessage()), exception);
		}
	}

	public static void disableAutoCommit(Connection connection) {
		try {
			if(connection != null) {
				connection.setAutoCommit(false);
			}
		} catch (Exception exception) {
			LOGGER.warn(LoggerUtil.formatMessage("Exception ::: {0}", exception.getMessage()), exception);
		}
	}

	public static void commitTransaction(Connection connection) {
		try {
			if(connection != null) {
				connection.commit();
			}
		} catch (Exception exception) {
			LOGGER.warn(LoggerUtil.formatMessage("Exception ::: {0}", exception.getMessage()), exception);
		}
	}

	public static void rollbackTransaction(Connection connection) {
		try {
			if(connection != null) {
				connection.rollback();
			}
		} catch (Exception exception) {
			LOGGER.warn(LoggerUtil.formatMessage("Exception ::: {0}", exception.getMessage()), exception);
		}
	}

	public static void closeStatement(Statement statement) {
		try {
			if(statement != null) {
				statement.close();
			}
		} catch (Exception exception) {
			LOGGER.warn(LoggerUtil.formatMessage("Exception ::: {0}", exception.getMessage()), exception);
		}
	}

	public static void closeResultSet(ResultSet resultSet) {
		try {
			if(resultSet != null) {
				resultSet.close();
			}
		} catch (Exception exception) {
			LOGGER.warn(LoggerUtil.formatMessage("Exception ::: {0}", exception.getMessage()), exception);
		}
	}

	public static String getContent(final String filePath) throws IOException {
		try(FileReader fileReader = new FileReader(filePath)) {
			return getContent(fileReader);
		}
	}

	public static String getContent(final Reader reader) throws IOException {
		if(reader == null) {
			return null;
		}
		String eachLine = null;
		StringBuilder fileContent = new StringBuilder();
		try (BufferedReader bufferedReader = new BufferedReader(reader)) {
			while ((eachLine = bufferedReader.readLine()) != null) {
				fileContent.append(eachLine + "\n");
			}
		}
		return fileContent.toString();
	}

	public static void closeInputStream(InputStream inputStream) {
		try {
			if(inputStream != null) {
				inputStream.close();
			}
		} catch (Exception exception) {
			LOGGER.warn(LoggerUtil.formatMessage("Exception ::: {0}", exception.getMessage()), exception);
		}
	}

	public static Properties loadProperties(final String filePath) throws Exception {
		FileInputStream fileInputStream = null;
		try {
			Properties properties = new Properties();
			fileInputStream = new FileInputStream(filePath);
			properties.load(fileInputStream);
			return properties;
		} catch (Exception exception) {
			throw exception;
		} finally {
			closeInputStream(fileInputStream);
		}
	}

	public static String readFile(final InputStream inputStream) throws Exception {
		return org.apache.commons.io.IOUtils.toString(inputStream);
	}
}