package com.contactmanager.common.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 * @author siva-4578
 *
 */
public class XmlParser {

	private String filePath;
	private File file;
	private SAXReader reader;
	private Document document;

	private XmlParser(Builder builder) throws DocumentException {
		this.filePath = builder.filePath;
		this.file = new File(this.filePath);
		this.reader = new SAXReader();
		this.document = this.reader.read(this.file);
	}

	public String filePath() {
		return filePath;
	}

	public File getFile() {
		return file;
	}

	public SAXReader getReader() {
		return reader;
	}

	public Document getDocument() {
		return document;
	}

	public static class Builder {

		private String filePath;

		public static Builder getInstance() {
			return new Builder();
		}

		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

		public XmlParser build() throws DocumentException {
			return new XmlParser(this);
		}
	}


	/**
	 * 
	 * <p><code>getTableSchemata</code> parses the database schema and return the list of table creation queries</p>
	 * 
	 * @return {@link List}
	 * 		A List of table create queries
	 */
	public List<String> getTableSchemata() {

		@SuppressWarnings("unchecked")
		List<Node> tableNodes = getDocument().selectNodes("/tables/table");
		if(tableNodes == null || tableNodes.isEmpty()) {
			return null;
		}
		List<String> tableCreateQueries = new ArrayList<>();
		tableNodes.forEach(node -> {
			tableCreateQueries.add(node.selectSingleNode("query").getText());
		});
		return tableCreateQueries;
	}
}