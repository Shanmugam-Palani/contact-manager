package com.contactmanager.common.utils;

/**
 * @author siva-4578
 *
 */
public class LoggerUtil {

	public static String formatMessage(String message, Object...objects) {
		if(message == null || message.trim().isEmpty()) {
			return null;
		}
		if(objects == null || objects.length == 0) {
			return message;
		}
		int placeholder = 0;
		for(Object each : objects) {
			if(message.contains("{" + placeholder +"}")) {
				message = message.replace("{" + placeholder +"}", String.valueOf(each));
			}
			placeholder++;
		}

		return message;
	}
}