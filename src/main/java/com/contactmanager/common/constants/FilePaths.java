package com.contactmanager.common.constants;

import java.io.File;

/**
 * @author siva-4578
 *
 */
public enum FilePaths {
	WEBINF("WEB-INF"),
	CONF(WEBINF.getPath() + File.separator + "conf"),
	CONFIGURATION_PROPERTIES(CONF.getPath() + File.separator + "configuration.properties"),
	LOG4J_PROPERTIES(CONF.getPath() + File.separator + "log4j.properties"),
	LOGS_HOME("logs"),
	SCHEMA_CONFIGURATION(CONF.getPath() + File.separator + "ContactManagerSchema.xml");
	;


	private String path;

	private FilePaths(String path) {
		this.path = path;
	}

	public String getPath() {
		return this.path;
	}
}