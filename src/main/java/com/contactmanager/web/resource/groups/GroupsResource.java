package com.contactmanager.web.resource.groups;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.contactmanager.common.utils.ResponseUtil;
import com.contactmanager.contacts.objects.Group;
import com.contactmanager.contacts.processor.ContactsProcessor;

/**
 * @author siva-4578
 *
 */
public class GroupsResource {

	private static final Logger LOGGER = Logger.getLogger(GroupsResource.class);

	private GroupsResource() {

	}

	public static GroupsResource getInstance() throws Exception {
		return new GroupsResource();
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAllGroups(@QueryParam("searchString") String searchString) {
		try {
			List<Group> groups = searchString != null && !searchString.isEmpty() ? ContactsProcessor.getGroups(searchString) : ContactsProcessor.getGroups();
			return ResponseUtil.getSuccessResponse("groups", Group.toJSONArray(groups));
		} catch(Exception exception) {
			LOGGER.debug("Exception in GroupsResource:::getAllGroups", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}

	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	public Response createGroup(@FormParam("name") String name) {
		try {
			Group group = ContactsProcessor.addGroup(name);
			return ResponseUtil.getSuccessResponse("groups", group.toJSONObject());
		} catch(Exception exception) {
			LOGGER.debug("Exception in GroupsResource:::createGroup", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}
	
	@Path("contacts-summary")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getGroupContactsSummary() {
		try {
			return ResponseUtil.getSuccessResponse("summary", ContactsProcessor.getGroupsContactSummary());
		} catch(Exception exception) {
			LOGGER.debug("Exception in GroupsResource:::getGroupContactsSummary", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}

	@Path("{group_id}")
	public GroupResource getGroupResource(@PathParam("group_id") Long groupId) throws Exception {
		return GroupResource.getInstance(groupId);
	}
}