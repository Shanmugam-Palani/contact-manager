package com.contactmanager.web.resource.groups;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.contactmanager.common.utils.ResponseUtil;
import com.contactmanager.contacts.objects.Group;
import com.contactmanager.contacts.processor.ContactsProcessor;

/**
 * @author siva-4578
 *
 */
public class GroupResource {

	private static final Logger LOGGER = Logger.getLogger(GroupResource.class);

	private long groupId;

	private GroupResource(long groupId) {
		this.groupId = groupId;
	}

	public static GroupResource getInstance(long groupId) throws Exception {
		return new GroupResource(groupId);
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getGroup() {
		try {
			Group group = ContactsProcessor.getGroup(groupId);
			return ResponseUtil.getSuccessResponse("groups", group.toJSONObject());
		} catch(Exception exception) {
			LOGGER.debug("Exception in GroupResource:::getGroup", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}

	@PUT
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	public Response createGroup(@FormParam("name") String name) {
		try {
			Group group = ContactsProcessor.updateGroup(groupId, name);
			return ResponseUtil.getSuccessResponse("groups", group.toJSONObject());
		} catch(Exception exception) {
			LOGGER.debug("Exception in GroupsResource:::createGroup", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}

	@DELETE
	@Produces({MediaType.APPLICATION_JSON})
	public Response deleteGroup() {
		try {
			ContactsProcessor.deleteGroup(groupId);
			return ResponseUtil.getSuccessResponse("Group has been deleted");
		} catch(Exception exception) {
			LOGGER.debug("Exception in GroupResource:::deleteGroup", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}
}