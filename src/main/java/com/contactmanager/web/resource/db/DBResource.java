package com.contactmanager.web.resource.db;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.contactmanager.common.utils.ResponseUtil;
import com.contactmanager.db.DatabaseProcessor;

/**
 * @author siva-4578
 *
 */
public class DBResource {

	private static final Logger LOGGER = Logger.getLogger(DBResource.class);

	private DBResource() {

	}

	public static DBResource getInstance() throws Exception {
		return new DBResource();
	}

	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response createDatabase() {
		try {
			DatabaseProcessor.createDatabase();
			return ResponseUtil.getSuccessResponse("Database has been populated successfully");
		} catch(Exception exception) {
			LOGGER.debug("Exception in DBResource:::createDatabase", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}

	@DELETE
	@Produces({MediaType.APPLICATION_JSON})
	public Response deleteDatabase() {
		try {
			DatabaseProcessor.dropDatabase();
			return ResponseUtil.getSuccessResponse("Database has been deleted successfully");
		} catch(Exception exception) {
			LOGGER.debug("Exception in DBResource:::deleteDatabase", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}
}