//$Id$
/**
 * 
 */
package com.contactmanager.web.resource;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author karthi-4240
 *
 */
@ApplicationPath("cm") // No I18N
public class ApplicationConfigWeb extends ResourceConfig {
	
	public ApplicationConfigWeb() {
		setApplicationName("contact manager");
		packages(true, ApplicationConfigWeb.class.getPackage().getName());
	}
}
