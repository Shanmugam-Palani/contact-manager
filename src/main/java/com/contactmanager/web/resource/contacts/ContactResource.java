package com.contactmanager.web.resource.contacts;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.contactmanager.common.utils.ResponseUtil;
import com.contactmanager.contacts.objects.Contact;
import com.contactmanager.contacts.objects.ContactConfig;
import com.contactmanager.contacts.processor.ContactsProcessor;

/**
 * @author siva-4578
 *
 */
public class ContactResource {
	
	private static final Logger LOGGER = Logger.getLogger(ContactResource.class);
	
	private Long contactId;
	
	private ContactResource(Long contactId) {
		this.contactId = contactId;
	}
	
	public static ContactResource getInstance(Long contactId) throws Exception {
		return new ContactResource(contactId);
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getContact() {
		try {
			Contact contact = ContactsProcessor.getContact(contactId);
			return ResponseUtil.getSuccessResponse("contacts", contact.toJSONObject());
		} catch(Exception exception) {
			LOGGER.debug("Exception in ContactResource:::getContact", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}
	
	@PUT
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	public Response updateContact(@FormParam("name") String name, @FormParam("company") String company, @FormParam("email") String email, 
			@FormParam("phone") String phone, @FormParam("address") String address, @FormParam("groupId") Long groupId) {
		try {
			ContactConfig config = ContactConfig.getInstance();
			config.setName(name);
			config.setCompany(company);
			config.setEmail(email);
			config.setPhone(phone);
			config.setAddress(address);
			config.setGroupId(groupId);
			
			Contact contact = ContactsProcessor.updateContact(contactId, config);
			return ResponseUtil.getSuccessResponse("contacts", contact.toJSONObject());
		} catch(Exception exception) {
			LOGGER.debug("Exception in ContactResource:::updateContact", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}
	
	@DELETE
	@Produces({MediaType.APPLICATION_JSON})
	public Response deleteContact() {
		try {
			ContactsProcessor.deleteContact(contactId);
			return ResponseUtil.getSuccessResponse("Contact has been deleted");
		} catch(Exception exception) {
			LOGGER.debug("Exception in ContactResource:::deleteContact", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}
}