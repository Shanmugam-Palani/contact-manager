package com.contactmanager.web.resource.contacts;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.contactmanager.common.utils.ResponseUtil;
import com.contactmanager.contacts.objects.Contact;
import com.contactmanager.contacts.objects.ContactConfig;
import com.contactmanager.contacts.processor.ContactsProcessor;

/**
 * @author siva-4578
 *
 */
public class ContactsResource {
	
	private static final Logger LOGGER = Logger.getLogger(ContactsResource.class);
	
	private ContactsResource() {
		
	}
	
	public static ContactsResource getInstance() throws Exception {
		return new ContactsResource();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAllContacts(@QueryParam("searchString") String searchString, @QueryParam("groupId") Long groupId) {
		try {
			List<Contact> contacts;
			if (groupId != null && searchString != null) {
				contacts = ContactsProcessor.getContacts(groupId, searchString);
			} else if (groupId != null) {
				contacts = ContactsProcessor.getContacts(groupId);
			} else if (searchString != null) {
				contacts = ContactsProcessor.getContacts(searchString);
			} else {
				contacts = ContactsProcessor.getContacts();
			}
			return ResponseUtil.getSuccessResponse("contacts", Contact.toJSONArray(contacts));
		} catch(Exception exception) {
			LOGGER.debug("Exception in ContactsResource:::getAllContacts", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	public Response createContact(@FormParam("name") String name, @FormParam("company") String company, @FormParam("email") String email, 
			@FormParam("phone") String phone, @FormParam("address") String address, @FormParam("groupId") Long groupId) {
		try {
			ContactConfig config = ContactConfig.getInstance();
			config.setName(name);
			config.setCompany(company);
			config.setEmail(email);
			config.setPhone(phone);
			config.setAddress(address);
			config.setGroupId(groupId);
			
			Contact contact = ContactsProcessor.addContact(config);
			return ResponseUtil.getSuccessResponse("contacts", contact.toJSONObject());
		} catch(Exception exception) {
			LOGGER.debug("Exception in ContactsResource:::createContact", exception);
			return ResponseUtil.getFailureResponse(exception);
		}
	}
	
	@Path("{contact_id}")
	public ContactResource getContactResource(@PathParam("contact_id") Long contactId) throws Exception {
		return ContactResource.getInstance(contactId);
	}
}