//$Id$
package com.contactmanager.web.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.contactmanager.web.resource.contacts.ContactsResource;
import com.contactmanager.web.resource.db.DBResource;
import com.contactmanager.web.resource.groups.GroupsResource;

/**
 * @author karthi-4240
 *
 */
@Path("/") // No I18N
public class HomeResource {
	
	@Path("db")
	public DBResource getDatabaseResource() throws Exception {
		return DBResource.getInstance();
	}
	
	@Path("groups")
	public GroupsResource getGroupsResource() throws Exception {
		return GroupsResource.getInstance();
	}
	
	@Path("contacts")
	public ContactsResource getContactsResource() throws Exception {
		return ContactsResource.getInstance();
	}
	
	@GET
	@Path("test")// No I18N
	@Produces({MediaType.APPLICATION_JSON})
	public Response test() {
		JSONObject response = new JSONObject();
		response.put("msg", "Hello World!");
		
		return Response.ok().entity(response.toString()).build();
	}
	
}
