package com.contactmanager.joda;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * @author siva-4578
 *
 */
public class JodaAPI {

	private static final DateTimeZone DEFAULT_TIMEZONE = DateTimeZone.UTC;

	/**
	 * Getting current time in the UTC timezone
	 * @return millis
	 */
	public static long getCurrentTime() {
		return new DateTime(DEFAULT_TIMEZONE).getMillis();
	}

	/**
	 * Convert time millis to a requested pattern
	 * @param millis
	 * @param pattern
	 * @return Converted date
	 */
	public static String convertDate(long millis, String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		Date date = new Date(millis);
		return formatter.format(date);
	}

	/**
	 * Formatting time to default pattern
	 * @param millis
	 * @return Formatted date
	 */
	public static String format(long millis) {
		return format(millis, false);
	}

	/**
	 * Formatting time to default pattern, rounding to a day
	 * @param millis
	 * @param isDay
	 * @return Formatted date
	 */
	public static String format(long millis, boolean isDay) {
		return convertDate(millis, ((isDay) ? "yyyy-MM-dd" : "yyyy-MM-dd HH:mm:ss"));
	}
}