package com.contactmanager.contacts.processor;

import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.contactmanager.contacts.constants.ContactStatus;
import com.contactmanager.contacts.constants.GroupStatus;
import com.contactmanager.contacts.objects.Contact;
import com.contactmanager.contacts.objects.ContactConfig;
import com.contactmanager.contacts.objects.Group;
import com.contactmanager.contacts.operations.ContactManager;
import com.contactmanager.contacts.operations.IContactManager;

/**
 * @author siva-4578
 *
 */
public class ContactsProcessor {

	private ContactsProcessor() {

	}

	private static IContactManager getContactManager() throws Exception {
		return ContactManager.getInstance();
	}

	public static Group addGroup(String name) throws Exception {
		validateGroupName(name);
		validateDuplicateGroupName(name);
		return getContactManager().addGroup(name);
	}

	public static Group getGroup(long id) throws Exception {
		Group group = getContactManager().getGroup(id);
		validateGroup(group);
		return group;
	}

	public static Group getGroup(String name) throws Exception {
		validateGroupName(name);
		Group group = getContactManager().getGroup(name);
		validateGroup(group);
		return group;
	}

	public static Group updateGroup(long id, String name) throws Exception {
		validateGroupName(name);
		Group group = getContactManager().getGroup(id);
		validateGroup(group);
		validateDuplicateGroupName(name, id);
		group = getContactManager().updateGroup(id, name);
		return group;
	}

	public static void deleteGroup(long id) throws Exception {
		Group group = getContactManager().getGroup(id);
		validateGroup(group);
		getContactManager().deleteGroup(id);
	}

	public static List<Group> getGroups() throws Exception {
		return getContactManager().getGroups();
	}

	public static List<Group> getGroups(String searchString) throws Exception {
		validateSearchString(searchString);
		return getContactManager().getGroups(searchString);
	}
	
	public static JSONArray getGroupsContactSummary() throws Exception {
		JSONArray summaryJSON = new JSONArray();
		LinkedHashMap<String, Integer> summaryMap = getContactManager().getGroupsContactSummary();
		if (summaryMap == null || summaryMap.isEmpty()) {
			return summaryJSON;
		}
		
		for (String groupName : summaryMap.keySet()) {
			JSONObject groupSummary = new JSONObject();
			groupSummary.put("name", groupName);
			groupSummary.put("no_of_contacts", summaryMap.get(groupName));
			summaryJSON.put(groupSummary);
		}
		
		return summaryJSON;
	}

	public static Contact addContact(ContactConfig config) throws Exception {
		validateContactConfig(config);
		return getContactManager().addContact(config);
	}

	public static Contact getContact(long id) throws Exception {
		Contact contact = getContactManager().getContact(id);
		validateContact(contact);
		return contact;
	}

	public static List<Contact> getContacts() throws Exception {
		return getContactManager().getContacts();
	}

	public static List<Contact> getContacts(String searchString) throws Exception {
		validateSearchString(searchString);
		return getContactManager().getContacts(searchString);
	}

	public static List<Contact> getContacts(long groupId) throws Exception {
		validateGroup(getGroup(groupId));
		return getContactManager().getContacts(groupId);
	}

	public static List<Contact> getContacts(long groupId, String searchString) throws Exception {
		validateSearchString(searchString);
		validateGroup(getGroup(groupId));
		return getContactManager().getContacts(groupId, searchString);
	}

	public static Contact updateContact(long id, ContactConfig config) throws Exception {
		validateContactConfig(config, true);
		Contact contact = getContactManager().getContact(id);
		validateContact(contact);
		return getContactManager().updateContact(id, config);
	}

	public static void deleteContact(long id) throws Exception {
		Contact contact = getContactManager().getContact(id);
		validateContact(contact);
		getContactManager().deleteContact(id);
	}

	private static void validateContactConfig(ContactConfig config) throws Exception {
		validateContactConfig(config, false);
	}

	private static void validateContactConfig(ContactConfig config, boolean isUpdate) throws Exception {
		if (config == null) {
			throw new Exception("Insuffient param to add a contact");
		}
		config.validate(isUpdate);
	}

	private static void validateGroupName(String name) throws Exception {
		if (name == null || name.trim().isEmpty()) {
			throw new Exception("Group name shouldn't be left blank");
		}
	}

	private static void validateDuplicateGroupName(String name) throws Exception {
		validateDuplicateGroupName(name, null);
	}

	private static void validateDuplicateGroupName(String name, Long id) throws Exception {
		if ((id != null ? getContactManager().isDuplicateGroup(name, id) : getContactManager().isDuplicateGroup(name))) {
			throw new Exception("Group with same name already exists");
		}
	}

	private static void validateGroup(Group group) throws Exception {
		if (group == null) {
			throw new Exception("No such group");
		}

		if (group.getStatus() != GroupStatus.ACTIVE) {
			throw new Exception("Group has been deleted already");
		}
	}

	private static void validateContact(Contact contact) throws Exception {
		if (contact == null) {
			throw new Exception("No such contact");
		}

		if (contact.getStatus() != ContactStatus.ACTIVE) {
			throw new Exception("Contact has been deleted already");
		}
	}

	private static void validateSearchString(String searchString) throws Exception {
		if (searchString == null || searchString.trim().isEmpty()) {
			throw new Exception("Search string shouldn't be left empty");
		}
	}
}