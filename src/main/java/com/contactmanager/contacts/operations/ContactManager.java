package com.contactmanager.contacts.operations;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.contactmanager.common.utils.IOUtils;
import com.contactmanager.common.utils.LoggerUtil;
import com.contactmanager.contacts.constants.ContactStatus;
import com.contactmanager.contacts.constants.GroupStatus;
import com.contactmanager.contacts.objects.Contact;
import com.contactmanager.contacts.objects.ContactConfig;
import com.contactmanager.contacts.objects.Group;
import com.contactmanager.joda.JodaAPI;
import com.contactmanager.threadlocal.CMThreadLocal;

/**
 * @author siva-4578
 *
 */
public class ContactManager implements IContactManager {

	private static final Logger LOGGER = Logger.getLogger(ContactManager.class);

	private static ContactManager contactManager = null;

	public static ContactManager getInstance() {
		if (contactManager == null) {
			contactManager = new ContactManager();
		}

		return contactManager;
	}

	@Override
	public Group addGroup(String name) throws Exception {
		PreparedStatement statement = null;
		ResultSet record = null;
		try {
			String query = "INSERT INTO Groups (NAME, CREATED_TIME, MODIFIED_TIME) "
					+ "VALUES(?, ?, ?)";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, name);
			long currentTime = JodaAPI.getCurrentTime();
			statement.setLong(2, currentTime);
			statement.setLong(3, currentTime);
			statement.executeUpdate();
			record = statement.getGeneratedKeys();
			if (record.next()) {
				return getGroup(record.getLong(1));
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in addGroup ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(record);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return null;
	}

	@Override
	public Group getGroup(long id) throws Exception {
		PreparedStatement statement = null;
		ResultSet record = null;
		try {
			String query = "SELECT * FROM Groups WHERE GROUP_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setLong(1, id);
			record = statement.executeQuery();
			if (record.next()) {
				return Group.recordToObj(record);
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getGroup ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(record);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return null;
	}

	@Override
	public Group getGroup(String name) throws Exception {
		PreparedStatement statement = null;
		ResultSet record = null;
		try {
			String query = "SELECT * FROM Groups WHERE NAME = ? AND STATUS = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setString(1, name);
			statement.setInt(2, GroupStatus.ACTIVE.getId());
			record = statement.executeQuery();
			if (record.next()) {
				return Group.recordToObj(record);
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getGroup ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(record);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return null;
	}

	@Override
	public List<Group> getGroups() throws Exception {
		List<Group> groups = null;
		PreparedStatement statement = null;
		ResultSet records = null;
		try {
			String query = "SELECT * FROM Groups WHERE STATUS = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setInt(1, GroupStatus.ACTIVE.getId());
			records = statement.executeQuery();
			groups = new ArrayList<Group>();
			while (records.next()) {
				groups.add(Group.recordToObj(records));
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getGroups ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(records);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return groups;
	}

	@Override
	public List<Group> getGroups(String searchString) throws Exception {
		List<Group> groups = null;
		PreparedStatement statement = null;
		ResultSet records = null;
		try {
			String query = "SELECT * FROM Groups WHERE NAME LIKE ? AND STATUS = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setString(1, getGlobalLikeMatchString(searchString));
			statement.setInt(2, GroupStatus.ACTIVE.getId());
			records = statement.executeQuery();
			groups = new ArrayList<Group>();
			while (records.next()) {
				groups.add(Group.recordToObj(records));
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getGroups ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(records);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return groups;
	}

	@Override
	public Group updateGroup(long id, String name) throws Exception {
		PreparedStatement statement = null;
		try {
			String query = "UPDATE Groups SET NAME = ?, MODIFIED_TIME = ? WHERE GROUP_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setString(1, name);
			statement.setLong(2, JodaAPI.getCurrentTime());
			statement.setLong(3, id);
			statement.executeUpdate();

			return getGroup(id);
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in updateGroup ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}

	@Override
	public void deleteGroup(long id) throws Exception {
		PreparedStatement statement = null;
		try {
			removeContactGroupId(id);
			String query = "DELETE FROM Groups WHERE GROUP_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in deleteGroup ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}

	private void removeContactGroupId(long id) throws Exception {
		PreparedStatement statement = null;
		try {
			String query = "UPDATE Contacts SET GROUP_ID = NULL, MODIFIED_TIME = ? WHERE GROUP_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setLong(1, JodaAPI.getCurrentTime());
			statement.setLong(2, id);
			statement.executeUpdate();
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in updateGroup ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}

	@Override
	public boolean isDuplicateGroup(String name) throws Exception {
		return isDuplicateGroup(name, null);
	}

	@Override
	public boolean isDuplicateGroup(String name, Long id) throws Exception {
		PreparedStatement statement = null;
		ResultSet record = null;
		try {
			if (id != null) {
				String query = "SELECT * FROM Groups WHERE NAME = ? AND STATUS = ? AND GROUP_ID != ?";
				statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
				statement.setString(1, name);
				statement.setInt(2, GroupStatus.ACTIVE.getId());
				statement.setLong(3, id);
			} else {
				String query = "SELECT * FROM Groups WHERE NAME = ? AND STATUS = ?";
				statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
				statement.setString(1, name);
				statement.setInt(2, GroupStatus.ACTIVE.getId());
			}

			record = statement.executeQuery();

			return record.next();
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getGroup ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(record);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}
	
	@Override
	public LinkedHashMap<String, Integer> getGroupsContactSummary() throws Exception {
		PreparedStatement statement = null;
		ResultSet records = null;
		try {
			String query = "select Groups.NAME, count(CONTACT_ID) from Groups left join Contacts on Groups.GROUP_ID = Contacts.GROUP_ID where Groups.STATUS = ? group by Contacts.GROUP_ID order by Groups.NAME";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setInt(1, GroupStatus.ACTIVE.getId());

			records = statement.executeQuery();
			
			LinkedHashMap<String, Integer> summaryMap = new LinkedHashMap<String, Integer>();
			while (records.next()) {
				String groupName = records.getString(1);
				int count = records.getInt(2);
				summaryMap.put(groupName, count);
			}

			return summaryMap;
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getGroupsContactSummary ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(records);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}

	@Override
	public Contact addContact(ContactConfig config) throws Exception {
		PreparedStatement statement = null;
		ResultSet record = null;
		try {
			String query = "INSERT INTO Contacts (NAME, COMPANY, EMAIL, PHONE, ADDRESS, GROUP_ID, CREATED_TIME, MODIFIED_TIME) "
					+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, config.getName());
			statement.setString(2, config.getCompany());
			statement.setString(3, config.getEmail());
			statement.setString(4, config.getPhone());
			statement.setString(5, config.getAddress());
			statement.setLong(6, config.getGroupId());
			long currentTime = JodaAPI.getCurrentTime();
			statement.setLong(7, currentTime);
			statement.setLong(8, currentTime);
			statement.executeUpdate();
			record = statement.getGeneratedKeys();
			if (record.next()) {
				return getContact(record.getLong(1));
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in addContact ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(record);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return null;
	}

	@Override
	public Contact getContact(long id) throws Exception {
		PreparedStatement statement = null;
		ResultSet record = null;
		try {
			String query = "SELECT * FROM Contacts WHERE CONTACT_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setLong(1, id);
			record = statement.executeQuery();
			if (record.next()) {
				return Contact.recordToObj(record);
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getContact ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(record);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return null;
	}

	@Override
	public List<Contact> getContacts() throws Exception {
		List<Contact> contacts = null;
		PreparedStatement statement = null;
		ResultSet records = null;
		try {
			String query = "SELECT * FROM Contacts WHERE STATUS = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setInt(1, ContactStatus.ACTIVE.getId());
			records = statement.executeQuery();
			contacts = new ArrayList<Contact>();
			while (records.next()) {
				contacts.add(Contact.recordToObj(records));
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getContacts ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(records);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return contacts;
	}

	@Override
	public List<Contact> getContacts(String searchString) throws Exception {
		List<Contact> contacts = null;
		PreparedStatement statement = null;
		ResultSet records = null;
		try {
			String query = "SELECT * FROM Contacts WHERE (NAME LIKE ? OR COMPANY LIKE ? OR EMAIL LIKE ? OR PHONE LIKE ? OR ADDRESS LIKE ?) AND STATUS = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			String likeMatcher = getGlobalLikeMatchString(searchString);
			statement.setString(1, likeMatcher);
			statement.setString(2, likeMatcher);
			statement.setString(3, likeMatcher);
			statement.setString(4, likeMatcher);
			statement.setString(5, likeMatcher);
			statement.setInt(6, ContactStatus.ACTIVE.getId());
			records = statement.executeQuery();
			contacts = new ArrayList<Contact>();
			while (records.next()) {
				contacts.add(Contact.recordToObj(records));
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getContacts ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(records);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return contacts;
	}

	@Override
	public List<Contact> getContacts(long groupId) throws Exception {
		List<Contact> contacts = null;
		PreparedStatement statement = null;
		ResultSet records = null;
		try {
			String query = "SELECT * FROM Contacts WHERE STATUS = ? AND GROUP_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setInt(1, ContactStatus.ACTIVE.getId());
			statement.setLong(2, groupId);
			records = statement.executeQuery();
			contacts = new ArrayList<Contact>();
			while (records.next()) {
				contacts.add(Contact.recordToObj(records));
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getContacts ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(records);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return contacts;
	}

	@Override
	public List<Contact> getContacts(long groupId, String searchString) throws Exception {
		List<Contact> contacts = null;
		PreparedStatement statement = null;
		ResultSet records = null;
		try {
			String query = "SELECT * FROM Contacts WHERE (NAME LIKE ? OR COMPANY LIKE ? OR EMAIL LIKE ? OR PHONE LIKE ? OR ADDRESS LIKE ?) AND STATUS = ? AND GROUP_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			String likeMatcher = getGlobalLikeMatchString(searchString);
			statement.setString(1, likeMatcher);
			statement.setString(2, likeMatcher);
			statement.setString(3, likeMatcher);
			statement.setString(4, likeMatcher);
			statement.setString(5, likeMatcher);
			statement.setInt(6, ContactStatus.ACTIVE.getId());
			statement.setLong(7, groupId);
			records = statement.executeQuery();
			contacts = new ArrayList<Contact>();
			while (records.next()) {
				contacts.add(Contact.recordToObj(records));
			}
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in getContacts ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeResultSet(records);
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}

		return contacts;
	}

	@Override
	public Contact updateContact(long id, ContactConfig config) throws Exception {
		PreparedStatement statement = null;
		try {
			List<String> updateColumns = getUpdatableContactColumnsQuery(config);
			String query = "UPDATE Contacts " + StringUtils.join(updateColumns, ",") + " MODIFIED_TIME = ? WHERE CONTACT_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setLong(1, JodaAPI.getCurrentTime());
			statement.setLong(2, id);
			statement.executeUpdate();

			return getContact(id);
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in updateContact ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}

	private List<String> getUpdatableContactColumnsQuery(ContactConfig config) throws Exception {
		List<String> updateColumns = new ArrayList<String>();
		if (config.getName() != null) {
			updateColumns.add("SET NAME = '"+ config.getName() + "'" );
		}
		if (config.getCompany() != null) {
			updateColumns.add("SET COMPANY = '"+ config.getCompany() + "'" );
		}
		if (config.getEmail() != null) {
			updateColumns.add("SET EMAIL = '"+ config.getEmail() + "'" );
		}
		if (config.getPhone() != null) {
			updateColumns.add("SET PHONE = '"+ config.getPhone() + "'" );
		}
		if (config.getAddress() != null) {
			updateColumns.add("SET ADDRESS = '"+ config.getAddress() + "'" );
		}
		if (config.getGroupId() != null) {
			updateColumns.add("SET GROUP_ID = "+ config.getGroupId());
		}
		return updateColumns;
	}

	@Override
	public void deleteContact(long id) throws Exception {
		PreparedStatement statement = null;
		try {
			String query = "DELETE FROM Contacts WHERE CONTACT_ID = ?";
			statement = CMThreadLocal.getConnector().getConnection().prepareStatement(query);
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (Exception exception) {
			LOGGER.debug(LoggerUtil.formatMessage("Problem in deleteContact ::: {0}", new Object[] {exception.getMessage()}), exception);
			throw exception;
		} finally {
			IOUtils.closeStatement(statement);
			CMThreadLocal.getConnector().close();
		}
	}
	
	private String sanitizeLikeSearchString(String searchString) throws Exception {
		if (searchString == null) {
			return searchString;
		}
		searchString = searchString.replace("!", "!!").replace("%", "!%").replace("_", "!_").replace("[", "![");

		return searchString;
	}
	
	private String getGlobalLikeMatchString(String searchString) throws Exception {
		if (searchString == null) {
			return searchString;
		}
		searchString = sanitizeLikeSearchString(searchString);

		return "%" + searchString + "%";
	}
}