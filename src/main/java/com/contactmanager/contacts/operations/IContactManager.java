package com.contactmanager.contacts.operations;

import java.util.LinkedHashMap;
import java.util.List;

import com.contactmanager.contacts.objects.Contact;
import com.contactmanager.contacts.objects.ContactConfig;
import com.contactmanager.contacts.objects.Group;

/**
 * @author siva-4578
 *
 */
public interface IContactManager {

	/**
	 * Creates a new group
	 * @param name
	 * @return {@link Group}
	 * @throws Exception
	 */
	public Group addGroup(String name) throws Exception;

	/**
	 * Get a group of an identifier
	 * @param id
	 * @return {@link Group}
	 * @throws Exception
	 */
	public Group getGroup(long id) throws Exception;

	/**
	 * Get a group of a name
	 * @param name
	 * @return {@link Group}
	 * @throws Exception
	 */
	public Group getGroup(String name) throws Exception;

	/**
	 * Get all available groups
	 * @return {@link List<Group>}
	 * @throws Exception
	 */
	public List<Group> getGroups() throws Exception;

	/**
	 * Get all available groups whose name matches the search string
	 * @param searchString
	 * @return {@link List<Group>}
	 * @throws Exception
	 */
	public List<Group> getGroups(String searchString) throws Exception;

	/**
	 * Update a group of an identifier with a new name
	 * @param id
	 * @param name
	 * @return {@link Group}
	 * @throws Exception
	 */
	public Group updateGroup(long id, String name) throws Exception;

	/**
	 * Delete a group of an identifier
	 * @param id
	 * @throws Exception
	 */
	public void deleteGroup(long id) throws Exception;

	/**
	 * Veirfy whether a group is duplicate or not
	 * @param name
	 * @return {@link Boolean}
	 * @throws Exception
	 */
	public boolean isDuplicateGroup(String name) throws Exception;

	/**
	 * Veirfy whether a group is duplicate or not excluding an identifier
	 * @param name
	 * @param id
	 * @return {@link Boolean}
	 * @throws Exception
	 */
	public boolean isDuplicateGroup(String name, Long id) throws Exception;
	
	/**
	 * Get group based contacts count summary
	 * @return {@link LinkedHashMap} contacts count summary
	 * @throws Exception
	 */
	public LinkedHashMap<String, Integer> getGroupsContactSummary() throws Exception;

	/**
	 * Creates a new contact
	 * @param config
	 * @return {@link Contact}
	 * @throws Exception
	 */
	public Contact addContact(ContactConfig config) throws Exception;

	/**
	 * Get a contact of an identifier
	 * @param id
	 * @return {@link Contact}
	 * @throws Exception
	 */
	public Contact getContact(long id) throws Exception;

	/**
	 * Get all available contacts
	 * @return {@link List<Contact>}
	 * @throws Exception
	 */
	public List<Contact> getContacts() throws Exception;

	/**
	 * Get all available contacts whose one of the fields matches the search string
	 * @param searchString
	 * @return {@link List<Contact>}
	 * @throws Exception
	 */
	public List<Contact> getContacts(String searchString) throws Exception;

	/**
	 * Get all available contacts of a group
	 * @param groupId
	 * @return {@link List<Contact>}
	 * @throws Exception
	 */
	public List<Contact> getContacts(long groupId) throws Exception;

	/**
	 * Get all available contacts of a group whose one of the fields matches the search string
	 * @param groupId
	 * @param searchString
	 * @return {@link List<Contact>}
	 * @throws Exception
	 */
	public List<Contact> getContacts(long groupId, String searchString) throws Exception;

	/**
	 * Update a contact of an identifier
	 * @param id
	 * @param config
	 * @return {@link Contact}
	 * @throws Exception
	 */
	public Contact updateContact(long id, ContactConfig config) throws Exception;

	/**
	 * Delete a contact of an identifier
	 * @param id
	 * @throws Exception
	 */
	public void deleteContact(long id) throws Exception;
}