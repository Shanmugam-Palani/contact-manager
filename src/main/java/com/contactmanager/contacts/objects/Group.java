package com.contactmanager.contacts.objects;

import java.sql.ResultSet;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.contactmanager.contacts.constants.GroupStatus;
import com.contactmanager.joda.JodaAPI;

/**
 * @author siva-4578
 *
 */
public class Group {

	private long id;
	private String name;
	private GroupStatus status;
	private long createdTime;
	private long modifiedTime;

	private Group(Builder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.status = builder.status;
		this.createdTime = builder.createdTime;
		this.modifiedTime = builder.modifiedTime;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public GroupStatus getStatus() {
		return status;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public long getModifiedTime() {
		return modifiedTime;
	}

	public JSONObject toJSONObject() {
		JSONObject group = new JSONObject();
		group.put("id", getId());
		group.put("name", getName());
		group.put("status", getStatus().toJSONObject());
		group.put("created_time", JodaAPI.format(getCreatedTime()));
		group.put("modified_time", JodaAPI.format(getModifiedTime()));

		return group;
	}
	
	public static JSONArray toJSONArray(List<Group> groupsList) throws Exception {
		JSONArray groupsArray = new JSONArray();
		if (groupsList != null && !groupsList.isEmpty()) {
			groupsList.forEach((group) -> groupsArray.put(group.toJSONObject()));
		}
		return groupsArray;
	}

	private static class Builder {
		private long id;
		private String name;
		private GroupStatus status;
		private long createdTime;
		private long modifiedTime;

		private static Builder getInstance() {
			return new Builder();
		}

		private void setId(long id) {
			this.id = id;
		}

		private void setName(String name) {
			this.name = name;
		}

		private void setStatus(GroupStatus status) {
			this.status = status;
		}

		private void setCreatedTime(long createdTime) {
			this.createdTime = createdTime;
		}

		private void setModifiedTime(long modifiedTime) {
			this.modifiedTime = modifiedTime;
		}

		private Group build() {
			return new Group(this);
		}
	}

	public static Group recordToObj(ResultSet record) throws Exception {
		if (record == null) {
			return null;
		}

		Group.Builder builder = Group.Builder.getInstance();
		builder.setId(record.getLong("GROUP_ID"));
		builder.setName(record.getString("NAME"));
		builder.setStatus(GroupStatus.getStatus(record.getInt("STATUS")));
		builder.setCreatedTime(record.getLong("CREATED_TIME"));
		builder.setModifiedTime(record.getLong("MODIFIED_TIME"));

		return builder.build();
	}
}