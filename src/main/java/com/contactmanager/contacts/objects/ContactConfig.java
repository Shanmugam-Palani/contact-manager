package com.contactmanager.contacts.objects;

/**
 * @author siva-4578
 *
 */
public class ContactConfig {
	
	private String name;
	private String company;
	private String email;
	private String phone;
	private String address;
	private Long groupId;
	
	private ContactConfig() {
		
	}
	
	public static ContactConfig getInstance() {
		return new ContactConfig();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	public void validate(boolean isUpdate) throws Exception {
		if (isUpdate) {
			if (!(name != null || company != null || email != null || phone != null || address != null || groupId != null)) {
				throw new Exception("At least one field should be to set to update a contact");
			}
		} else {
			if (name == null || name.trim().isEmpty()) {
				throw new Exception("Contact name shouldn't be left blank");
			}
		}
	}
}