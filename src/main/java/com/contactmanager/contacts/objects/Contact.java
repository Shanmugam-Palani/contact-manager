package com.contactmanager.contacts.objects;

import java.sql.ResultSet;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.contactmanager.contacts.constants.ContactStatus;
import com.contactmanager.joda.JodaAPI;

/**
 * @author siva-4578
 *
 */
public class Contact {

	private long id;
	private String name;
	private String company;
	private String email;
	private String phone;
	private String address;
	private Long groupId;
	private ContactStatus status;
	private long createdTime;
	private long modifiedTime;

	private Contact(Builder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.company = builder.company;
		this.email = builder.email;
		this.phone = builder.phone;
		this.address = builder.address;
		this.groupId = builder.groupId;
		this.status = builder.status;
		this.createdTime = builder.createdTime;
		this.modifiedTime = builder.modifiedTime;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCompany() {
		return company;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public Long getGroupId() {
		return groupId;
	}

	public ContactStatus getStatus() {
		return status;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public long getModifiedTime() {
		return modifiedTime;
	}

	public JSONObject toJSONObject() {
		JSONObject contact = new JSONObject();
		contact.put("id", getId());
		contact.put("name", getName());
		contact.put("company", getCompany());
		contact.put("email", getEmail());
		contact.put("phone", getPhone());
		contact.put("address", getAddress());
		contact.put("group_id", getGroupId());
		contact.put("status", getStatus().toJSONObject());
		contact.put("created_time", JodaAPI.format(getCreatedTime()));
		contact.put("modified_time", JodaAPI.format(getModifiedTime()));

		return contact;
	}

	public static JSONArray toJSONArray(List<Contact> contactsList) throws Exception {
		JSONArray contactsArray = new JSONArray();
		if (contactsList != null && !contactsList.isEmpty()) {
			contactsList.forEach((contact) -> contactsArray.put(contact.toJSONObject()));
		}
		return contactsArray;
	}

	private static class Builder {
		private long id;
		private String name;
		private String company;
		private String email;
		private String phone;
		private String address;
		private Long groupId;
		private ContactStatus status;
		private long createdTime;
		private long modifiedTime;

		private static Builder getInstance() {
			return new Builder();
		}

		private void setId(long id) {
			this.id = id;
		}

		private void setName(String name) {
			this.name = name;
		}

		private void setCompany(String company) {
			this.company = company;
		}

		private void setEmail(String email) {
			this.email = email;
		}

		private void setPhone(String phone) {
			this.phone = phone;
		}

		private void setAddress(String address) {
			this.address = address;
		}

		private void setGroupId(Long groupId) {
			this.groupId = groupId;
		}

		private void setStatus(ContactStatus status) {
			this.status = status;
		}

		private void setCreatedTime(long createdTime) {
			this.createdTime = createdTime;
		}

		private void setModifiedTime(long modifiedTime) {
			this.modifiedTime = modifiedTime;
		}

		private Contact build() {
			return new Contact(this);
		}
	}

	public static Contact recordToObj(ResultSet record) throws Exception {
		if (record == null) {
			return null;
		}

		Contact.Builder builder = Contact.Builder.getInstance();
		builder.setId(record.getLong("CONTACT_ID"));
		builder.setName(record.getString("NAME"));
		builder.setCompany(record.getString("COMPANY"));
		builder.setEmail(record.getString("EMAIL"));
		builder.setPhone(record.getString("PHONE"));
		builder.setAddress(record.getString("ADDRESS"));
		builder.setGroupId(record.getLong("GROUP_ID"));
		builder.setStatus(ContactStatus.getStatus(record.getInt("STATUS")));
		builder.setCreatedTime(record.getLong("CREATED_TIME"));
		builder.setModifiedTime(record.getLong("MODIFIED_TIME"));

		return builder.build();
	}
}