package com.contactmanager.contacts.constants;

import org.json.JSONObject;

/**
 * @author siva-4578
 *
 */
public enum ContactStatus {

	ACTIVE(0, "Active"),
	DELETED(1, "Deleted"),
	;

	private int id;
	private String name;

	private ContactStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public JSONObject toJSONObject() {
		JSONObject status = new JSONObject();
		status.put("id", getId());
		status.put("name", getName());
		
		return status;
	}

	public static ContactStatus getStatus(int id) {
		for (ContactStatus status : ContactStatus.values()) {
			if (status.getId() == id) {
				return status;
			}
		}

		return null;
	}
}